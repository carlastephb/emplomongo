<?php

require_once(__DIR__.'/../model/Employees.php');
require_once(__DIR__.'/../model/db/EmployeesDb.php');

class IndexController{

    public function listEmployees(){
        $db = new EmployeesDB();
        $emp = $db->listEmployees();
        return $emp;
    }

    public function detailsEmployees($pos = 0){
        $db = new EmployeesDb();
        return $db->getEmployees($pos); // ens selecciona un sol empleat
     }

    public function createEmployees($nom, $cog, $dne, $tip, $fot, $sou){
        $db = new EmployeesDb();
        return $db->insertEmployees($nom, $cog, $dne, $tip, $fot, $sou); 
     }

    public function updateEmployees($id, $nom, $cog, $dne, $tip, $sou){
        $db = new EmployeesDB();
        return $db->updateRegEmployees($id, $nom, $cog, $dne, $tip, $sou);
     }

    public function deleteEmployees($eid){
        $db = new EmployeesDb();
        $emp = $db->getEmployees($eid);
        $db->removeEmployees($eid);
        return $emp;
    }

    public function setMainImage($eid, $img){
        $db = new EmployeesDb();
        return $db->insertEmployees($nom, $cog, $dne, $tid, $tip, $fot, $sou);
    }

    public function getEmployeeTypes(){
        $db = new EmployeesDb();
        return $db->listEmployeeTypes();
    }
}
