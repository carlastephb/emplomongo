<?php

class Employees{

    private $_id; // amb modif. de visibilitat i si son priv comenc _ (asi no salen en el autocompletado)
    
    private $_nom;
    private $_cognoms;
    private $_dataneixement;
    private $_idtipus;
    private $_tipus;
    private $_foto;
    private $_sou;

    public function __construct($i = null,$n,$c,$d,$it,$t,$f,$s){ // parametros de entrada de cada una de las var. datos del trab.
        if($i != null){
            $this->setId($i);
        }
        $this->setNom($n);
        $this->setCognoms($c);
        $this->setDataneixement($d);
        $this->setIdTipus($it)($it);
        $this->setTipus($t);
        $this->setFoto($f);
        $this->setSou($s);
    }

    // getters

    public function getId(){
        return $this->_id;
    }

    public function getNom(){
        return $this->_nom;
    }

    public function getCognoms(){
        return $this->_cognoms;
    }

    public function getDataneixement(){
        return $this->_dataneixement;
    }

    public function getIdTipus(){
        return $this->_idtipus;
    }

    public function getTipus(){
        return $this->_tipus;
    }

    public function getFoto(){
        return $this->_foto;
    }

    public function getSou(){
        return $this->_sou;
    }

    // setters

    public function setId($i){
        $this->_id = $i;
    }

    public function setNom($n){
        $this->_nom = $n;
    }

    public function setCognoms($c){
        $this->_cognoms = $c;
    }

    public function setDataneixement($d){
        $this->_dataneixement = $d;
    }

    public function setIdTipus($it){
        $this->_idtipus = $it;
    }

    public function setTipus($ii){
        $this->_tipus = $ii;
    }

    public function setFoto($f){
        $this->_foto = $f;
    }

    public function setSou($s){
        $this->_sou = $s;
    }

    public function toArray(){ // en retorna un array pel .json
        
        if($this->getId() != null){
            $employee['id'] = $this->getId();
        }
        $employee = [
            "name" => $this->getNom(),
            "cognoms" => $this->getCognoms(),
            "dataneixement" => $this->getDataneixement(),
            "tipus" => [
                "idtipus" => $this->getIdTipus(),
                "tipus" => $this->getTipus()
            ],
            "foto" => $this->getFoto(),
            "sou" => $this->getSou()
        ];
        return $employee;
    }
}
