<?php

// require: si no troba app para i once: perque nomes s'importi una vegada
require_once(__DIR__.'/../../ext/vendor/autoload.php'); 
require_once(__DIR__.'/../../inc/constants.php'); 
require_once(__DIR__.'/../Employees.php'); 
require_once(__DIR__.'/../../inc/ConnectionManager.php');

class EmployeesDb{

    private $_conn;

    // Retorna un array d'instancies 
    public function listEmployees(){ 
      $cm = ConnectionManager::getConnectionManager();
      $collection = $cm->connectEmployees();
      $es = $collection->find();

      $employees = array();
      foreach($es as $e){
        array_push($employees, new Employees($e['_id']->__toString(), $e['name'], $e['cognoms'], $e['dataneixement'], $e['tipus']['idtipus'], 
              $e['tipus']['tipus'], $e['foto'], $e['sou'])); // afegim instancies
      }
      return $employees;
    }

    // Retorna el tipus d'Empleat. array mixt on les claus son idtype and nametype
    public function listEmployeeTypes(){
      $cm = ConnectionManager::getConnectionManager();
      $collection = $cm->connectEmployeeType();
      $ets = $collection->find();

      $employeestypes = array();
      foreach($ets as $et){
        array_push($employeestypes, ["idtipus" => $et['_id']->__toString(), "tipus" => $et['employee_type']]);
      }
      return $employeestypes;
    }

    // Retorna un tipus de planta

    public function getEmployeeType($id){
      $cm = ConnectionManager::getConnectionManager();
      $collection = $cm->connectEmployeeType();
      $et = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
      
      return ["idtipus" => $et['_id']->__toString(), "tipus"=> $et['employee_type']];
    }

    // Retorna un empleat
    public function getEmployees($id){ // rep id del employee que volem
      $cm = ConnectionManager::getConnectionManager();
      $collection = $cm->connectEmployees();
      $e = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
  
      return new Employees($e['_id']->__toString(), $e['name'], $e['cognoms'], $e['dataneixement'], $e['tipus']['idtipus'], 
      $e['tipus']['tipus'], $e['foto'], $e['sou']);
    }

    // Retorna un empleat instanciat 
    public function insertEmployees($n, $c, $d, $t, $f, $s){
      $collection = $cm->connectEmployees();
      $type = $this->getEmployeeType($t);

      // no puc afegir el idtipus ------------------ SEGURAMENTE MAL  -----------------------  ! ! ! ! ! ! ! ! ! ! ! 
      $employee = new Employee($n,$c,$d,$t,$type['tipus'],$f,$s);
      $insertE = $collection->insertOne($employee->toArray());
      
      return $this->getEmployees($insertE->getInsertedId());
    }

    // Retorna un empleat
    public function updateRegEmployees($id, $n, $c, $d, $t, $s){
      $type = $this->getEmployeeType($t);
      $employee = new Employee($n, $c, $d, $it, $type['tipus'], $f,$s);
      $updatestmt = [ '$set' => $employee->toArray() ];
      $updatefilter =  ['_id' => new MongoDB\BSON\ObjectId($id)];
      $cm = ConnectionManager::getConnectionManager();
      $collection = $cm->connectEmployees();
  
      $updatep = $collection->updateOne($updatefilter, $updatestmt);
      return $this->getEmployees($id);
    }
  
    // Retorna un empleat
    public function removeEmployees($id){ // hauria de ser singular "employee"
      $cm = ConnectionManager::getConnectionManager();
      $collection = $cm->connectEmployees();
      $employee = $this->getEmployees($id);
  
      $removep = $collection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
      return $employee;
    }
    
    public function updateImage($id, $image){

    }

    // Retorna la conexio en forma de valors
    private function openConnection(){
      return (new MongoDB\Client)->employees->employee;
    }
  
  }
  
