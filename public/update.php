<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

$cnt = new IndexController();
$fs = $cnt->detailsEmployees($_GET['index']);

?><html>
<head>
  <title>Update Page</title>
</head>
<body>
  <h1>Update Employee</h1>
  <form id="eform" method="post" action="/forms/update.php">
  <dl>  
        <dt><label for="adde-nom">Nom</label></dt>
        <dd><input type="text" id="adde-nom" name="emnom" tabindex="1" value="<?=$fs->getNom()?>"/></dd>
        <dt><label for="adde-cognoms">Cognoms</label></dt>
        <dd><input type="text" id="adde-cognoms" name="emcog" tabindex="2" value="<?=$fs->getCognoms()?>"/></dd>
        <dt><label for="adde-dataneixement">Data neixement</label></dt>
        <dd><input type="text" id="adde-dataneixement" name="emdn" tabindex="3" value="<?=$fs->getDataneixement()?>"/></dd>
        <dt><label for="adde-dataneixement">Id Tipus</label></dt>
        <dd><input type="numeric" id="adde-dataneixement" name="emdn" tabindex="4" value="<?=$fs->getIdTipus()?>"/></dd>
        <dt><label for="adde-posicio">Tipus Posicio</label></dt>
        <dd>
            <select id="adde-posicio" name="empo" tabindex="5">
                <option value="Xef"<?php if($fs->getTipus() == "Xef"){ echo " selected";} ?>>Xef</option>
                <option value="Encarregat de cuina"<?php if($fs->getTipus() == "Encarregat de cuina"){ echo " selected";} ?>>Encarregat de cuina</option>
                <option value="Encarregat/ada de sala"<?php if($fs->getTipus() == "Encarregat/ada de sala"){ echo " selected";} ?>>Encarregat/ada de sala</option>
                <option value="Cambrer/a"<?php if($fs->getTipus() == "Cambrer/a"){ echo " selected";} ?>>Cambrer/a</option>
                <option value="Cuiner/a"<?php if($fs->getTipus() == "Cuiner/a"){ echo " selected";} ?>>Cuiner/a</option>
                <option value="Ajudant de sala"<?php if($fs->getTipus() == "Ajudant de sala"){ echo " selected";} ?>>Ajudant de sala</option>
                <option value="Ajudant de cuina"<?php if($fs->getTipus() == "Ajudant de cuina"){ echo " selected";} ?>>Ajudant de cuina</option>
            </select>
        </dd>
        <dt><label for="adde-sou">Sou</label></dt>
        <dd><input type="numeric" id="adde-sou" name="emsou" tabindex="6" value="<?=$fs->getSou()?>"/></dd>
        <dt><input type="hidden" name="emid" value="<?=$fs->getId()?>"/></dt>
        <dd><input type="submit" value="Update" name="emsub" /></dd> 
    </dl>
  </form>
  <td>
      <button type="button" class="btn btn-indigo btn-sm m-0"><a href="/">Back to home</a></button>
  </td>
</body>
</html>
