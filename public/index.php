<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

$cnt = new IndexController();
$fs = $cnt->listEmployees();

?><html>
<div class="jumbotron text-center">
  <h1>Agenda d'Empresa</h1>
  <h4>Treballadors del Restaurant Luki's</h4>
</div>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Agenda Luki's</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Employee <b>Details</b></h2></div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i><a href="/add.php">  Add Employee</a></button>
                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($fs as $f){ ?>
                    <tr>
                    <td><a href="/details.php?index=<?=$f->getId()?>">
                      <?=$f->getNom()?>
                      <?=$f->getCognoms()?>
                    </a></td>
                        <td>   
                            <button type="button" class="btn btn-outline-success"><a href="/update.php?index=<?=$f->getId()?>">Update</a></button>
                            <button type="button" class="btn btn-outline-success"><a href="/delete.php?index=<?=$f->getId()?>">Delete</a></button>
                        </td>               
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>     
</body>
</html>