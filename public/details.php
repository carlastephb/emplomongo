<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

$cnt = new IndexController();
$fs = $cnt->detailsEmployees($_GET['index']);

?><html>
<head>
  <title>Details</title>
  <meta charset="utf-6">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<div class="jumbotron text-center">
  <h1>Detalls Empleat</h1>
</div>

</head>
    <body>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-8"></div>
                    </div>
                </div>
                <table class="table table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Birthday</th>
                            <th>Ocupation Id</th>
                            <th>Ocupation Type</th>
                            <th>Salary</th>
                            <th>Photo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?=$fs->getNom()?></td>
                            <td><?=$fs->getCognoms()?></td>
                            <td><?=$fs->getDataneixement()?></td>
                            <td><?=$fs->getIdTipus()?></td>
                            <td><?=$fs->getTipus()?></td>
                            <td><?=$fs->getSou()?></td>
                            <td></div>
                                <?php if($fs->getFoto() != NULL){ ?>
                                <img src="<?=UPLOAD_HTTP?><?=$fs->getFoto()?>" height="145" width="120"/>
                                <?php } ?></td>     
                        </tr>
                    </tbody>
                    <td>
                    <button type="button" class="btn btn-indigo btn-sm m-0"><a href="/">Back to home</a></button>
                    </td>   
                </table>
            </div>
        </div> 
    </body>
</html>