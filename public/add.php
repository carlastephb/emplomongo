<html>
  <head>
    <title>Add Employee</title>
  </head>
  <body>
    <h1>Add Employee</h1>
    <form id="emform" method="post" action="/forms/add.php" enctype="multipart/form-data">
    <dl>  
        <dt><label for="adde-nom">Nom</label></dt>
        <dd><input type="text" id="adde-nom" name="emnom" tabindex="1"/></dd>
        <dt><label for="adde-cognoms">Cognoms</label></dt>
        <dd><input type="text" id="adde-cognoms" name="emcog" tabindex="2"/></dd>
        <dt><label for="adde-dataneixement">Data neixement</label></dt>
        <dd><input type="text" id="adde-dataneixement" name="emdn" tabindex="3"/></dd>
        <dt><label for="adde-idtipus">Id Tipus Posicio</label></dt>
        <dd><input type="numeric" id="adde-idtipus" name="emiti" tabindex="4"/></dd>
        <dt><label for="adde-tipus">Tipus Posicio</label></dt>
        <dd>
            <select id="adde-tipus" name="emtip" tabindex="5">
                <option value="Xef" selected>Xef</option>
                <option value="Encarregat de cuina" selected>Encarregat de cuina</option>
                <option value="Encarregat/ada de sala" selected>Encarregat/ada de sala</option>
                <option value="Cambrer/a" selected>Cambrer/a</option>
                <option value="Cuiner/a" selected>Cuiner/a</option>
                <option value="Ajudant de sala" selected>Ajudant de sala</option>
                <option value="Ajudant de cuina" selected>Ajudant de cuina</option>
            </select>
        </dd>
        <dt><label for="adde-foto">Foto</label></dt>
        <dd><input type="file" id="adde-foto" name="emfo" tabindex="6"/></dd>
        <dt><label for="adde-sou">Sou</label></dt>
        <dd><input type="numeric" id="adde-sou" name="emsou" tabindex="7"/></dd>
        <dt>&nbsp;</dt>
        <dd><input type="submit" value="Add" name="emsub" /></dd> 
    </dl>
    </form>
    <a href="/">Back to home</a>
  </body>
</html>