<?php

require_once(__DIR__.'/../../app/inc/constants.php');
require_once(__DIR__.'/../../app/controller/IndexController.php');

$neid = $_POST['emid'];
$nnom = $_POST['emnom'];
$ncog = $_POST['emcog'];
$ndat = $_POST['emdn'];
$niti = $_POST['emiti'];
$ntip = $_POST['emtip'];
$nsou = $_POST['emsou'];

$cnt = new IndexController();
$fs = $cnt->updateEmployees($neid, $nnom, $ncog, $ndat, $niti, $ntip, $nsou) ;

?><html>
<head>
  <title>Update Page</title>
  <meta charset="utf-6">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<html>
<head>
  <title>Updated Employee</title>
</head>
<body>
  <h1>Updated Employee: <?=$fs->getNom()?></h1>
    <ul>
      <li><?=$fs->getCognoms()?>
      <li><?=$fs->getDataneixement()?>
      <li><?=$fs->getIdTipus()?>
      <li><?=$fs->getTipus()?>
      <li><?=$fs->getFoto()?>
      <li><?=$fs->getSou()?>
    </ul>
    <td>
      <button type="button" class="btn btn-indigo btn-sm m-0"><a href="/">Back to home</a></button>
    </td>
</body>
</html>
