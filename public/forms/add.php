<?php

require_once(__DIR__.'/../../app/inc/constants.php');
require_once(__DIR__.'/../../app/controller/IndexController.php');

$nnom = $_POST['emnom'];
$ncog = $_POST['emcog'];
$ndat = $_POST['emdn'];
$niti = $_POST['emiti'];
$ntip = $_POST['emtip'];
$nsou = $_POST['emsou'];

move_uploaded_file($_FILES['emfo']['tmp_name'], UPLOAD_DIR.$_FILES['emfo']['name']);

$cnt = new IndexController();
$fs = $cnt->createEmployees($nnom, $ncog, $ndat, $niti, $ntip, $_FILES['emfo']['name'], $nsou);

?><html>
<head>
  <title>Add Page</title>
</head>

<div class="jumbotron text-center">
  <h4>El treballador s'ha afegit a l'agenda</h4>
</div>

<html>
  <body>
  <h1><?=$fs->getNom()?></h1>
    <ul>
      <li><?=$fs->getCognoms()?>
      <li><?=$fs->getDataneixement()?>
      <li><?=$fs->getIdTipus()?>
      <li><?=$fs->getTipus()?>
      <li><?=$fs->getFoto()?>
      <li><?=$fs->getSou()?>
    </ul>
    <td>
      <button type="button" class="btn btn-indigo btn-sm m-0"><a href="/">Back to home</a></button>
    </td>
  </body>
</html>
