<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

$cnt = new IndexController();
$fs = $cnt->detailsEmployees($_GET['index']);

?><html>
<head>
  <title>Delete page</title>
  <meta charset="utf-6">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<div class="jumbotron text-center">
  <h4>Vols eliminar el seguent treballador de l'agenda?</h4>
</div>

<html>
  <head>
    <title>Delete Employee</title>
  </head>
  <body>
    <div class="container">
        <h2><?=$fs->getNom()?></h2>
            <ul>
              <li><?=$fs->getCognoms()?>
              <li><?=$fs->getDataneixement()?>
              <li><?=$fs->getIdTipus()?>
              <li><?=$fs->getTipus()?>
              <li><?=$fs->getFoto()?>
              <li><?=$fs->getSou()?>
            </ul>
        <td> <button type="button" class="btn btn-indigo btn-sm m-0"><a href="/forms/delete.php?index=<?=$_GET['index']?>">Delete</a><br/></button></td>
        <td><button type="button" class="btn btn-indigo btn-sm m-0"><a href="/">Back to home</a></button></td>
    </div>
  </body>
</html>